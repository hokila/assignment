import XCTest
@testable import assignment

class MockBasicAPI: BasicAPIDelegate {
    func cancelRequest(_ identifier:String,callback:@escaping ()->Void) {
        
    }
    
    func sendRequest(_ method:APIMethod, url:URL, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        
    }
    
    func sendRequest(_ method:APIMethod, url:URL, identifier:String, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        
    }
    
    var completion:([String:Any]?,Error?) = (nil, nil)
    
    func cancelThenSendRequest(_ method:APIMethod, url:URL, identifier:String, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        callback(completion.0, completion.1)
    }
}

class MockAPI:APILoginDelegate, APIUpdateDelegate {
    var loginCallback = Result.success([String:Any]())
    func login(callback:@escaping APICompletion) {
        callback(loginCallback)
    }
    
    var updateCallback = Result.success([String:Any]())
    func update(timezone:String,callback:@escaping APICompletion) {
        callback(updateCallback)
    }
}

class MockLoginViewController: LoginPresenterDelegate {
    private(set) var isDidNotLogin = false
    func didNotLogin() {
        isDidNotLogin = true
    }
    
    private(set) var isDidLogin = false
    func didLogin() {
        isDidLogin = true
    }
}

class MockUpdateViewController: UpdatePresenterDelegate {
    private(set) var isDidUpdateSuccess = false
    func didUpdateSuccess() {
        isDidUpdateSuccess = true
    }
    
    private(set) var isDidUpdateFailure = false
    func didUpdateFailure() {
        isDidUpdateFailure = true
    }
}
