
import XCTest
@testable import assignment

class LoginPresenterTest: BaseTest {
    private var mockAPI = MockAPI()
    private var loginPresenter:LoginPresenter!
    private var vc = MockLoginViewController()
    private let apiError = NSError(domain: "", code: 0, userInfo: nil)
    
    override func setUp() {
        super.setUp()
        mockAPI = MockAPI()
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testInit() {
        loginPresenter = LoginPresenter()
        loginPresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidNotLogin)
        loginPresenter.launch()
        XCTAssertTrue(vc.isDidNotLogin)
    }
    
    func testLoginSuccess() {
        mockAPI.loginCallback = Result.success([String:Any]())
        
        loginPresenter = LoginPresenter(apiDelegate: mockAPI)
        loginPresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidLogin)
        loginPresenter.pressLogin()
        XCTAssertTrue(vc.isDidLogin)
    }
    
    func testLoginFail() {
        mockAPI.loginCallback = Result.failure(apiError)
        
        loginPresenter = LoginPresenter(apiDelegate: mockAPI)
        loginPresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidLogin)
        loginPresenter.pressLogin()
        XCTAssertFalse(vc.isDidLogin)
    }
}

class UpdatePresenterTest: BaseTest {
    private var mockAPI = MockAPI()
    private var updatePresenter:UpdatePresenter!
    private var vc = MockUpdateViewController()
    private let apiError = NSError(domain: "", code: 0, userInfo: nil)
    
    override func setUp() {
        super.setUp()
        mockAPI = MockAPI()
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testInit() {
        updatePresenter = UpdatePresenter()
        updatePresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidUpdateSuccess)
        XCTAssertFalse(vc.isDidUpdateFailure)
    }
    
    func testUpdateSuccess() {
        mockAPI.updateCallback = Result.success([String:Any]())
        updatePresenter = UpdatePresenter(apiDelegate: mockAPI)
        updatePresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidUpdateSuccess)
        updatePresenter.pressUpdate(value: "6")
        XCTAssertTrue(vc.isDidUpdateSuccess)
    }
    
    func testUpdateFail() {
        mockAPI.updateCallback = Result.failure(apiError)
        updatePresenter = UpdatePresenter(apiDelegate: mockAPI)
        updatePresenter.delegate = vc
        
        XCTAssertFalse(vc.isDidUpdateFailure)
        updatePresenter.pressUpdate(value: "6")
        XCTAssertTrue(vc.isDidUpdateFailure)
    }
}
