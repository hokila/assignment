
import XCTest
@testable import assignment

class APITests: BaseTest {
    private var mockBasicAPI = MockBasicAPI()
    private var api:API!
    private var expection = XCTestExpectation()
    
    override func setUp() {
        super.setUp()
        expection = XCTestExpectation()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoginSuccess() {
        mockBasicAPI.completion = (loadJSON(fileName: "loginSuccess", type: "json"), nil )
        api = API(shareAPI: mockBasicAPI)
        api.login { (result) in
            XCTAssertTrue(result.isSuccess())
            XCTAssertNotNil(self.api.user)
            
            self.expection.fulfill()
        }
        
        wait(for: [expection], timeout: 1.0)
    }
    
    func testLoginFailure() {
        mockBasicAPI.completion = (nil, NSError(domain: "", code: 0, userInfo: nil) )
        api = API(shareAPI: mockBasicAPI)
        api.login { (result) in
            XCTAssertTrue(result.isFailure())
            XCTAssertNil(self.api.user)
            
            self.expection.fulfill()
        }
        
        wait(for: [expection], timeout: 1.0)
    }
    
    private func setUser() {
        api.user = User(loadJSON(fileName: "loginSuccess", type: "json"))
    }
    
    func testUpdateSuccess() {
        mockBasicAPI.completion = (loadJSON(fileName: "updateSuccess", type: "json"), nil )
        api = API(shareAPI: mockBasicAPI)
        setUser()
        
        api.update(timezone: "0") { (result) in
            XCTAssertTrue(result.isSuccess())
            self.expection.fulfill()
        }
        
        wait(for: [expection], timeout: 1.0)
    }
    
    func testUpdateFailure() {
        mockBasicAPI.completion = (nil, NSError(domain: "", code: 0, userInfo: nil) )
        api = API(shareAPI: mockBasicAPI)
        setUser()
        
        api.update(timezone: "0") { (result) in
            XCTAssertTrue(result.isFailure())
            
            self.expection.fulfill()
        }
        
        wait(for: [expection], timeout: 1.0)
    }
}
