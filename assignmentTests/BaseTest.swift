
import XCTest

class BaseTest: XCTestCase {
    
    func loadJSON(fileName:String,type:String) -> [String:Any]? {
        
        guard let path = Bundle(for: BaseTest.self).path(forResource: fileName, ofType: type) else {
            XCTFail("invalid path, forget set target? ")
            return nil
        }
        
        guard let loadData = NSData(contentsOfFile: path)  else {
            XCTFail("not load jsonData")
            return nil
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: loadData as Data, options: []) as? [String: Any] else {
            XCTFail("unable convert to json")
            return nil
        }
        
        return json
    }
    
}
