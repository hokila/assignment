
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    private let loginPresenter = LoginPresenter()
    private let updatePresenter = UpdatePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginPresenter.delegate = self
        updatePresenter.delegate = self
        
        loginPresenter.launch()
    }
    
    @IBAction func touchLogin(_ sender: Any) {
        loginPresenter.pressLogin()
    }
    
    @IBAction func touchUpdate(_ sender: Any) {
        updatePresenter.pressUpdate(value: "8")
    }
    
}

extension ViewController:LoginPresenterDelegate {
    func didNotLogin() {
        loginButton.isUserInteractionEnabled = true
        loginButton.backgroundColor = UIColor.yellow
    }
    
    func didLogin() {
        loginButton.isUserInteractionEnabled = false
        loginButton.backgroundColor = UIColor.white
    }
}

extension ViewController:UpdatePresenterDelegate {
    func didUpdateSuccess() {
        print("didUpdateSuccess")
    }
    
    func didUpdateFailure() {
        print("didUpdateFailure")
    }
}

