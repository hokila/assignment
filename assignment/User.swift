
class User {
    let objectId:String
    let sessionToken:String
    let username:String
    
    init?(_ json:[String:Any]?) {
        guard let json = json else{
            return nil
        }
    
        guard let objectId = json["objectId"] as? String, let sessionToken = json["sessionToken"] as? String else{
            return nil
        }
        
        self.objectId = objectId
        self.sessionToken = sessionToken
        
        self.username = (json["username"] as? String) ?? ""
    }
}
