
import Foundation

protocol LoginPresenterDelegate:class {
    func didNotLogin()
    func didLogin()
}

class LoginPresenter {
    
    weak var delegate:LoginPresenterDelegate?
    
    private var loginApi:APILoginDelegate
    
    init(apiDelegate:APILoginDelegate = API.shareInstance) {
        self.loginApi = apiDelegate
    }
    
    func launch() {
        self.delegate?.didNotLogin()
    }
    
    func pressLogin () {
        loginApi.login { (result) in
            switch result {
            case .success(_):
                self.delegate?.didLogin()
                print("success")
            case .failure(_):
                self.delegate?.didNotLogin()
                print("failure")
            }
        }
    }
}
