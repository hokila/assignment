

import Foundation


/*
 Basic API module
 No business logic
 Can replace to other Network module like AFnetworking, Almofire
 */

enum APIMethod:String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

typealias BasicAPICompletion = ([String:Any]?,Error?) -> Void

class BasicAPI:NSObject, URLSessionTaskDelegate {
    
    private let tRequestTimeout:TimeInterval = 30.0
    private let tResourceTimeout:TimeInterval = 60*60*24
    private let maximumConnectionsPerHost = 20
    
    lazy var defaultSessionConfiguration:URLSessionConfiguration = {
        let sessionConfig = URLSessionConfiguration.ephemeral
        
        sessionConfig.timeoutIntervalForRequest = tRequestTimeout
        sessionConfig.timeoutIntervalForResource = tResourceTimeout
        sessionConfig.httpMaximumConnectionsPerHost = maximumConnectionsPerHost
        return sessionConfig
    }()
    
    fileprivate let defaultHeader = ["content-type":"application/json"]
    fileprivate let generalIdentifier = "API_general"
    
    fileprivate var operationQueue:OperationQueue?
    fileprivate var mainSession:URLSession!
    
    
    var errorDomain = "APIErrorDomain"
    
    override init() {
        super.init()
        
        let myConfiguration = self.defaultSessionConfiguration
        self.operationQueue = OperationQueue()
        self.mainSession = URLSession(configuration: myConfiguration, delegate:self, delegateQueue: self.operationQueue)
    }
}

extension BasicAPI:URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let trust = challenge.protectionSpace.serverTrust {
                let credential = URLCredential(trust: trust)
                completionHandler(.useCredential, credential)
            }
        }
    }
}

protocol BasicAPIDelegate {
    func cancelRequest(_ identifier:String,callback:@escaping ()->Void)
    func sendRequest(_ method:APIMethod, url:URL, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion )
    func sendRequest(_ method:APIMethod, url:URL, identifier:String, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion )
    func cancelThenSendRequest(_ method:APIMethod, url:URL, identifier:String, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion )
}

extension BasicAPI:BasicAPIDelegate {
    
    func cancelRequest(_ identifier:String,callback:@escaping ()->Void){
        
        mainSession.getTasksWithCompletionHandler({ (dataTask, uploadTask, downloadTask) -> Void in
            
            let allTasks = NSArray(array: dataTask)
            allTasks.addingObjects(from: uploadTask)
            allTasks.addingObjects(from: downloadTask)
            
            if allTasks.count > 0{
                for task:URLSessionTask in allTasks as! [URLSessionDataTask]{
                    if task.taskDescription == identifier{
                        task.cancel()
                    }
                }
            }
            
            callback()
        })
    }
    
    private func sendRequest(_ request:URLRequest, identifier:String, callback:@escaping BasicAPICompletion) {
        
        let task = mainSession.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if  error?._code == NSURLErrorCancelled {
                return
            }
        
            DispatchQueue.main.async {
                guard let dataFromNetworking = data, error == nil else {
//                    print(error!)
                    callback(nil,error)
                    return
                }
                
                guard let json = try? JSONSerialization.jsonObject(with: dataFromNetworking, options: []) as? [String: Any] else{
                    callback(nil,NSError(domain: self.errorDomain, code: 0, userInfo: ["message":"json parse fail"]))
                    return
                }
                
                //Success
                callback(json,nil)
            }
        })
        task.resume()
    }

    func sendRequest(_ method:APIMethod, url:URL, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        sendRequest(method, url: url, identifier: generalIdentifier, header: header, jsonBody: jsonBody, callback: callback)
    }
    
    func sendRequest(_ method:APIMethod, url:URL, identifier:String, header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        var request:URLRequest = URLRequest(url: url)
        
        // Method
        request.httpMethod = method.rawValue
        
        // Headers
        for dic in header {
            request.addValue(dic.value, forHTTPHeaderField:dic.key )
        }
        
        // Body
        let bodyString = jsonBody.queryParameters
        request.httpBody = bodyString.data(using: .utf8, allowLossyConversion: true)
        
        self.sendRequest(request, identifier: identifier, callback: callback)
    }
    
    func cancelThenSendRequest(_ method:APIMethod, url:URL, identifier:String = "API_identifier", header:[String:String], jsonBody:Dictionary<String,Any>, callback:@escaping BasicAPICompletion ) {
        cancelRequest(identifier) {
            self.sendRequest(method, url: url, identifier: identifier, header: header, jsonBody: jsonBody, callback: callback)
        }
    }
}

extension Dictionary {
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                              String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                              String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }
}

