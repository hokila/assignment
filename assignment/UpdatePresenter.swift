protocol UpdatePresenterDelegate:class {
    func didUpdateSuccess()
    func didUpdateFailure()
}

class UpdatePresenter {
    
    weak var delegate:UpdatePresenterDelegate?
    
    private var updateApi:APIUpdateDelegate
    
    init(apiDelegate:APIUpdateDelegate = API.shareInstance) {
        self.updateApi = apiDelegate
    }
    
    func pressUpdate (value:String) {
        
        updateApi.update(timezone: value) { (result) in
            switch result {
            case .success(_):
                self.delegate?.didUpdateSuccess()
            case .failure(_):
                self.delegate?.didUpdateFailure()
            }
        }
    }
}

