import Foundation

class API {
    static let shareInstance = API()
    
    private let applicationId = "vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD"
    private let domain = "https://watch-master-staging.herokuapp.com"
    
    #if UNITTEST
    var user:User?
    #else
    private(set) var user:User?
    #endif
    
    private let shareAPI:BasicAPIDelegate // use same API setting
    
    var defaultHeader:[String:String]  {
        get{
            return ["Content-Type":"application/x-www-form-urlencoded",
                    "X-Parse-Application-Id":applicationId,
                    "X-Parse-Revocable-Session":"1"]
        }
    }
    
    init(shareAPI:BasicAPIDelegate = BasicAPI()) {
        self.shareAPI = shareAPI
    }
}

/*****
 Assume only need consider success and failure case.
 Extend if need extra feature. Ex: status code is 500 and get json response data
 ****/

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

extension Result {
    func isSuccess() -> Bool {
        switch self {
        case .success(_):
            return true
        default:
            return false
        }
    }
    
    func isFailure() -> Bool {
        switch self {
        case .failure(_):
            return true
        default:
            return false
        }
    }
}

typealias APICompletion = (Result<[String:Any]>) -> Void

protocol APILoginDelegate {
    func login(callback:@escaping APICompletion)
}

extension API:APILoginDelegate {
    func login(callback:@escaping APICompletion) {
        let url = URL(string: "\(domain)/api/login")!

        let bodyParameters = ["username": "test2@qq.com","password": "test1234qq",]
        
        shareAPI.cancelThenSendRequest(.get, url: url, identifier: "loginAPI", header: defaultHeader, jsonBody: bodyParameters) { (json, error) in
            if let error = error {
                callback(.failure(error))
                return
            }
            
            if let json = json  {
                self.user = User(json)
                callback(.success(json))
            }
        }
    }
}

protocol APIUpdateDelegate {
    func update(timezone:String,callback:@escaping APICompletion)
}

extension API:APIUpdateDelegate {
    func update(timezone:String,callback:@escaping APICompletion) {
        guard let objectID = self.user?.objectId else {
            print("not get objectID yet")
            return
        }
        
        let url = URL(string: "\(domain)/api/users/\(objectID)")!
        let bodyParameters = ["timeZone": timezone]
        
        let identifier = "updateUser"
        
        shareAPI.cancelThenSendRequest(.put, url: url, identifier: identifier, header: defaultHeader, jsonBody: bodyParameters) { (json, error) in
            if let error = error {
                callback(.failure(error))
                return
            }
            
            if let json = json  {
                self.user = User(json)
                callback(.success(json))
            }
        } 
    }
}
